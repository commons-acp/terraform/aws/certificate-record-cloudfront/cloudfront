
resource "aws_acm_certificate_validation" "cert_app" {
  provider                = aws-cloudfront
  certificate_arn         = var.certif_arn
  validation_record_fqdns = [ for record in var.records : record.fqdn]
}

